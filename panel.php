<?php   
session_start();
require_once('constantes.php');

if ( !isset($_SESSION['login']) ){
  header('Location: index.php');
}
require_once('conexion.php');

$sql = 'select * from cuentas';
$result = $conn->query($sql);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Demostración para el curso de derecho informatico UMSA">
    <meta name="author" content="Grupo derecho">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Panel de administración</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="css/estilos.css" rel="stylesheet">
</head>

<body>
    <div id="container">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            <a class="navbar-brand" href="#"><?php echo APP_INST; ?></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="panel.php">Panel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="enviar.php">Enviar</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <a href="salir.php" class="btn btn-outline-success my-2 my-sm-0">Salir</a>
                </form>

            </div>
        </nav>
        
    </div>

    <div class="container">
            <h1>Listado de cuentas</h1>
            <table class="table table-responsive">
                <tr>
                    <th>Id</th>
                    <th>Paterno</th>
                    <th>Materno</th>
                    <th>Nombres</th>
                    <th>Celular</th>
                    <th>Cuenta</th>
                    <th>Tarjeta</th>
                    <th>Codigo</th>
                    <th></th>
                </tr>
                <?php 
                    if ($result->num_rows > 0) {        
                        $conta = 0;
                        while ($fila = $result->fetch_array()){
                            echo "<tr>";
                            echo '<td>' . $fila['id'] . '</td>';   
                            echo '<td>' . $fila['paterno'] . '</td>';
                            echo '<td>' . $fila['materno'] . '</td>';
                            echo '<td>' . $fila['nombres'] . '</td>';
                            echo '<td>' . $fila['celular'] . '</td>';
                            echo '<td>' . $fila['cuenta'] . '</td>';
                            echo '<td>' . $fila['tarjeta'] . '</td>';
                            echo '<td>' . $fila['codigo'] . '</td>';
                            echo '<td><a href="eliminar.php?id='. $fila['id'] . '" onclick="return confirm(\'Esta seguro\')">Eliminar</a></td>';
                            echo "</tr>";
                        }   
                    }
                    else{
                        echo '<p> No existen registros</p>';
                    }
                ?>
            </table>
    </div><!-- /.container -->

<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
