<?php 
session_start();
require_once 'constantes.php';
$result = '';
if ( isset($_SESSION['mensaje']) ){
    $result = $_SESSION['mensaje'];
    $_SESSION['mensaje'] = '';
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Demostración para el curso de derecho informatico UMSA">
    <meta name="author" content="<?php echo APP_INST; ?>">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Panel de administración</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">    

    <!-- Custom styles for this template -->
    <link href="css/estilos.css" rel="stylesheet">
</head>

<body>
    <div id="container">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            <a class="navbar-brand" href="#"><?php echo APP_INST ?></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="panel.php">Panel</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="enviar.php">Enviar</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <a href="salir.php" class="btn btn-outline-success my-2 my-sm-0">Salir</a>
                </form>

            </div>
        </nav>
        
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4 mt-5 bg-light rounded">
                <h1 class="text-center font-weight-bold text-primary">Enviar a:</h1>
                <hr class="bg-light">
                <h5 class="text-center text-success"><?php echo $result; ?></h5>
                <form action="procesar.php" method="post" id="form-box" class="p-2">
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                        </div>
                        <input type="email" name="email" class="form-control" placeholder="Escriba el correo electrónico" required>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submit" id="submit" class="btn btn-primary btn-block" value="Enviar">
                    </div>
                </form>
            </div>
        </div>

    </div><!-- /.container -->

<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
