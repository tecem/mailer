<?php 
session_start();
require_once('constantes.php');

if ( isset($_SESSION['login']) ){
  header('Location: panel.php');
}

if ( isset($_POST['ingresar']) ){
  if ($_POST['usuario'] == 'admin' && md5($_POST['password']) == '6ffa7d79b1add795f4a0d20f9dcb3358' ){
    $_SESSION['login'] = 'OK';
    header('Location: panel.php');
  }
}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Demostración para el curso de derecho informatico UMSA">
    <meta name="author" content="Grupo derecho">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Login</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">    
    <link rel="stylesheet" href="css/estilos.css">    

    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
  </head>

  <body class="text-center">
    <form class="form-signin" method="post">
      <img class="mb-4" src="../../assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal"><?php echo APP_NAME; ?></h1>
      <label for="usuario" class="sr-only">Usuario</label>
      <input type="text" name="usuario" id="usuario" class="form-control" placeholder="Usuario" required autofocus>
      <label for="password" class="sr-only">Contraseña</label>
      <input type="password" name="password" id="password" class="form-control" placeholder="Contraseña" required>
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Recuerdame
        </label>
      </div>
      <input type="submit" name="ingresar" class="btn btn-lg btn-primary btn-block" value="Ingresar">
      <p class="mt-5 mb-3 text-muted">&copy; 2018 Derecho UMSA</p>
    </form>
  </body>
</html>