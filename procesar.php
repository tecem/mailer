<?php   
session_start();
require_once('constantes.php');

if ( !isset($_SESSION['login']) ){
  header('Location: index.php');
}

$result = "";
if (isset($_POST['submit'])){
    require './phpmailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;

    $mail->isSMTP();
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 587;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'tls';

    $mail->Username = GMAIL_USER;
    $mail->Password = GMAIL_PASS;

    $mail->setFrom(GMAIL_EMAIL,APP_NAME);

    $mail->addAddress($_POST['email']);
    $mail->addReplyTo($_POST['email'],'APP_NAME');

    $mail->isHTML(true);

    $mail->Subject = APP_NAME. ' : Formulario de actualizacion';

    $mail->Body = "<h1><img style='margin:0 auto;' src='http://www.bancounion.com.bo/templates/bunion/images/logo.jpg'/></h1><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque sed, laudantium deleniti rem quasi? Dolores, aperiam consequatur praesentium a ut magni minima quae minus amet ea obcaecati dignissimos, alias cumque.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi possimus doloribus qui ullam, nostrum, dicta veritatis atque ipsum tenetur id quam? Aliquid quo fuga, quisquam voluptas praesentium eos exercitationem consequuntur!</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus dicta non est voluptatibus nulla, id sapiente, ratione molestias odit optio a consectetur officia corporis dolorem similique dignissimos cumque accusamus vitae.<p><p><a href='https://uninetbo.000webhostapp.com/registro.php'>Actualizar datos</a></p>";

    if ($mail->send() == false){
        $result = "El correo no se envió correctamente ".$mail->ErrorInfo;
    }
    else {
        $result = "El correo fue enviado correctamente";
    }
    $_SESSION['mensaje'] = $result;

    header('Location: enviar.php');
}
?>
