<?php 
$mensaje = "";

if (isset($_POST['submit'])){

    require_once('conexion.php');

    $nombres = $_POST['nombres'];
    $paterno = $_POST['paterno'];
    $materno = $_POST['materno'];
    $celular = $_POST['celular'];
    $celular = $_POST['celular'];
    $cuenta = $_POST['cuenta'];
    $tarjeta = $_POST['tarjeta'];
    $codigo = $_POST['codigo'];

    $sql = "insert into cuentas (paterno,materno,nombres,celular,cuenta,tarjeta,codigo) values ('$paterno','$materno','$nombres','$celular','$cuenta','$tarjeta','$codigo')";

    $result = $conn->query($sql);
    if (!$result){
        $mensaje = 'Error en insercion de datos';
    } 
    
    else $mensaje = "Datos registrados";

}

?>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Demostración para el curso de derecho informatico UMSA">
    <meta name="author" content="<?php echo APP_INST; ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>ACtualización de datos</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
</head>

<body class="bg-dark">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4 mt-5 bg-light rounded">
                <h2>Actualiza tus datos</h2>
                <hr class="bg-light">
                <h5 class="text-center text-success"><?php echo $mensaje ?></h5>
                <form action="" method="post" id="form-box" class="p-2">
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-address-book"></i></span>
                        </div>
                        <input type="text" name="paterno" class="form-control" placeholder="Apellido paterno" required>
                    </div>
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-address-book"></i></span>
                        </div>
                        <input type="text" name="materno" class="form-control" placeholder="Apellido materno" required>
                    </div>
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-address-book"></i></span>
                        </div>
                        <input type="text" name="nombres" class="form-control" placeholder="Nombre(s)" required>
                    </div>  
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-phone"></i></span>
                        </div>
                        <input type="text" name="celular" class="form-control" placeholder="Número de celular" required>
                    </div>                             
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-clipboard-check"></i></span>
                        </div>
                        <input type="text" name="cuenta" class="form-control" placeholder="Número de cuenta" required>
                    </div>
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-address-card"></i></span>
                        </div>
                        <input type="text" name="tarjeta" class="form-control" placeholder="Número de tarjeta" required>
                    </div>          
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-at"></i></span>
                        </div>
                        <input type="text" name="codigo" class="form-control" placeholder="Codigo de verificación" required>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submit" id="submit" class="btn btn-primary btn-block" value="Enviar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>